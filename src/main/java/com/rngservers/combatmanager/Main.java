package com.rngservers.combatmanager;

import com.rngservers.combatmanager.combat.CombatManager;
import com.rngservers.combatmanager.commands.CombatManagerCommand;
import com.rngservers.combatmanager.events.Events;
import com.rngservers.combatmanager.util.Vault;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Iterator;
import java.util.Map;

public final class Main extends JavaPlugin {
    CombatManager combatManager;

    @Override
    public void onEnable() {
        saveDefaultConfig();
        Vault vault = new Vault();
        if (!vault.setupEconomy()) {
            this.getLogger().severe("Disabled due to no Vault dependency found!");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
        Economy economy = vault.getEconomy();
        combatManager = new CombatManager(this, economy);
        this.getServer().getPluginManager().registerEvents(new Events(this, combatManager, economy), this);
        this.getCommand("combatmanager").setExecutor(new CombatManagerCommand(this));
    }

    @Override
    public void onDisable() {
        if (!combatManager.getCombat().isEmpty()) {
            for (Iterator<Map.Entry<Player, Long>> iterator = combatManager.getCombat().entrySet()
                    .iterator(); iterator.hasNext(); ) {
                if (iterator.hasNext()) {
                    Map.Entry<Player, Long> entry = iterator.next();
                    iterator.remove();
                    combatManager.removeCombat(entry.getKey());
                }
            }
        }
    }
}
