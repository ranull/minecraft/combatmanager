package com.rngservers.combatmanager.commands;

import com.rngservers.combatmanager.Main;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CombatManagerCommand implements CommandExecutor {
    private Main plugin;

    public CombatManagerCommand(Main plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        String version = "1.5";
        String author = "RandomUnknown";

        if (args.length < 1) {
            sender.sendMessage(ChatColor.DARK_GRAY + "» " + ChatColor.RED + "CombatManager " + ChatColor.GRAY
                    + ChatColor.GRAY + "v" + version);
            sender.sendMessage(ChatColor.GRAY + "/combatmanager " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET
                    + " Plugin info");
            if (sender.hasPermission("combatmanager.reload")) {
                sender.sendMessage(ChatColor.GRAY + "/combatmanager reload " + ChatColor.DARK_GRAY + "-"
                        + ChatColor.RESET + " Reload plugin");
            }
            sender.sendMessage(ChatColor.DARK_GRAY + "Author: " + ChatColor.GRAY + author);
            return true;
        }
        if (args.length == 1 && args[0].equals("reload")) {
            if (!sender.hasPermission("combatmanager.reload")) {
                sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.RED + "CombatManager" + ChatColor.DARK_GRAY
                        + "]" + ChatColor.RESET + " No Permission!");
                return true;
            }
            plugin.reloadConfig();
            sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.RED + "CombatManager" + ChatColor.DARK_GRAY
                    + "]" + ChatColor.RESET + " Reloaded config file!");
        }
        return true;
    }
}