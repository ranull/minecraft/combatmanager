package com.rngservers.combatmanager.events;

import com.rngservers.combatmanager.Main;
import com.rngservers.combatmanager.combat.CombatManager;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.List;

public class Events implements Listener {
    private Main plugin;
    private CombatManager combatManager;
    private Economy economy;

    public Events(Main plugin, CombatManager combatManager, Economy economy) {
        this.plugin = plugin;
        this.combatManager = combatManager;
        this.economy = economy;
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onAttack(EntityDamageByEntityEvent event) {
        if (event.isCancelled()) {
            return;
        }
        List<String> worlds = plugin.getConfig().getStringList("settings.worlds");
        List<String> combatEntities = plugin.getConfig().getStringList("settings.combatEntities");
        if (worlds.contains(event.getEntity().getLocation().getWorld().getName()) || worlds.contains("ALL")) {
            if (event.getEntity() instanceof Player) {
                if (!event.getEntity().hasPermission("combatmanager.bypass")) {
                    if (combatEntities.contains(event.getDamager().getType().toString()) || combatEntities.contains("ALL")) {
                        Player player = (Player) event.getEntity();
                        if (plugin.getConfig().getStringList("settings.gamemode").contains(player.getGameMode().toString())) {
                            combatManager.addCombat(player);
                        }
                    }
                }
            }
            if (event.getDamager() instanceof Player) {
                if (!event.getDamager().hasPermission("combatmanager.bypass")) {
                    if (combatEntities.contains(event.getEntity().getType().toString()) || combatEntities.contains("ALL")) {
                        Player player = (Player) event.getDamager();
                        if (plugin.getConfig().getStringList("settings.gamemode").contains(player.getGameMode().toString())) {
                            combatManager.addCombat(player);
                        }
                    }
                }
            }
            if (event.getDamager() instanceof Projectile) {
                Projectile projectile = (Projectile) event.getDamager();
                if (projectile.getShooter() instanceof LivingEntity) {
                    LivingEntity entity = (LivingEntity) projectile.getShooter();
                    if (!entity.hasPermission("combatmanager.bypass")) {
                        if (combatEntities.contains(entity.getType().toString()) || combatEntities.contains("ALL")) {
                            if (entity instanceof Player) {
                                Player player = (Player) entity;
                                if (plugin.getConfig().getStringList("settings.gamemode").contains(player.getGameMode().toString())) {
                                    combatManager.addCombat(player);
                                }
                            }
                            if (event.getEntity() instanceof Player) {
                                Player player = (Player) event.getEntity();
                                if (plugin.getConfig().getStringList("settings.gamemode").contains(player.getGameMode().toString())) {
                                    combatManager.addCombat(player);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        if (player.hasPermission("combatmanager.bypass")) {
            return;
        }
		List<String> worlds = plugin.getConfig().getStringList("settings.worlds");
        if (worlds.contains(player.getLocation().getWorld().getName()) || worlds.contains("ALL")) {
            if (combatManager.getCombat().containsKey(player)) {
                combatManager.removeCombat(player);
                combatManager.dropPlayer(player);
                combatManager.killPlayer(player);
                String logoutCombat = plugin.getConfig().getString("settings.logoutCombat")
                        .replace("$player", player.getDisplayName()).replace("&", "§");
                if (!logoutCombat.equals("")) {
                    player.getServer().broadcastMessage(logoutCombat);
                }
            }
        }
    }

    @EventHandler
    public void onItem(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (player.hasPermission("combatmanager.bypass")) {
            return;
        }
		List<String> worlds = plugin.getConfig().getStringList("settings.worlds");
        if (worlds.contains(player.getLocation().getWorld().getName()) || worlds.contains("ALL")) {
            if (combatManager.getCombat().containsKey(player)) {
                if (event.getItem() == null) {
                    return;
                }
                List<String> deniedItems = plugin.getConfig().getStringList("settings.deniedItems");
                if (deniedItems.contains(event.getItem().getType().toString())) {
                    event.setCancelled(true);
                    String denyItem = plugin.getConfig().getString("settings.denyItem").replace("&", "§");
                    if (!denyItem.equals("")) {
                        player.sendMessage(denyItem);
                    }
                    String denyItemTitle = plugin.getConfig().getString("settings.denyItemTitle")
                            .replace("&", "§");
                    String denyItemSubtitle = plugin.getConfig().getString("settings.denyItemSubtitle")
                            .replace("&", "§");
                    if (!denyItemTitle.equals("")) {
                        player.sendTitle(denyItemTitle, denyItemSubtitle, 10, 20, 10);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onTeleport(PlayerTeleportEvent event) {
        Player player = event.getPlayer();
        if (player.hasPermission("combatmanager.bypass")) {
            return;
        }
		List<String> worlds = plugin.getConfig().getStringList("settings.worlds");
        if (worlds.contains(player.getLocation().getWorld().getName()) || worlds.contains("ALL")) {
            if (combatManager.getCombat().containsKey(player)) {
                Boolean teleport = plugin.getConfig().getBoolean("settings.teleport");
                if (!teleport) {
                    event.setCancelled(true);
                    String denyTeleport = plugin.getConfig().getString("settings.denyTeleport").replace("&", "§");
                    if (!denyTeleport.equals("")) {
                        event.getPlayer().sendMessage(denyTeleport);
                    }
                    String denyTeleportTitle = plugin.getConfig().getString("settings.denyTeleportTitle")
                            .replace("&", "§");
                    String denyTeleportSubtitle = plugin.getConfig().getString("settings.denyTeleportSubtitle")
                            .replace("&", "§");
                    if (!denyTeleportTitle.equals("")) {
                        player.sendTitle(denyTeleportTitle, denyTeleportSubtitle, 10, 20, 10);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
		List<String> worlds = plugin.getConfig().getStringList("settings.worlds");
        if (worlds.contains(event.getEntity().getLocation().getWorld().getName()) || worlds.contains("ALL")) {
            if (combatManager.getCombat().containsKey(event.getEntity())) {
                combatManager.removeCombat(event.getEntity());
            }
        }
    }

    @EventHandler
    public void onDeathPlayer(PlayerDeathEvent event) {
		List<String> worlds = plugin.getConfig().getStringList("settings.worlds");
        if (worlds.contains(event.getEntity().getLocation().getWorld().getName()) || worlds.contains("ALL")) {
            if (event.getEntity().getKiller() instanceof Player) {
                String deathPlayerMoney = plugin.getConfig().getString("settings.deathPlayerMoney");
                Double money = economy.getBalance(event.getEntity());
                Double amount = combatManager.getMoneyAmount(event.getEntity(), deathPlayerMoney);
                if (amount <= 0 || money <= 0) {
                    return;
                }
                if (amount > money) {
                    amount = money;
                }
                economy.withdrawPlayer(event.getEntity(), amount);
                String deathPlayer = plugin.getConfig().getString("settings.deathPlayer")
                        .replace("$killer", event.getEntity().getKiller().getDisplayName())
                        .replace("$amount", amount.toString()).replace("&", "§");
                if (!deathPlayer.equals("")) {
                    event.getEntity().sendMessage(deathPlayer);
                }
                economy.depositPlayer(event.getEntity().getKiller(), amount);
                String deathKiller = plugin.getConfig().getString("settings.deathKiller")
                        .replace("$player", event.getEntity().getDisplayName())
                        .replace("$amount", amount.toString()).replace("&", "§");
                if (!deathKiller.equals("")) {
                    event.getEntity().getKiller().sendMessage(deathKiller);
                }
            }
        }
    }

    @EventHandler
    public void onDeathGeneral(PlayerDeathEvent event) {
        if (event.getEntity().getKiller() != null) {
            return;
        }
        if (event.getEntity().hasPermission("combatmanager.bypass")) {
            return;
        }
		List<String> worlds = plugin.getConfig().getStringList("settings.worlds");
        if (worlds.contains(event.getEntity().getLocation().getWorld().getName()) || worlds.contains("ALL")) {
            String deathMoney = plugin.getConfig().getString("settings.deathMoney");
            Double money = economy.getBalance(event.getEntity());
            Double amount = combatManager.getMoneyAmount(event.getEntity(), deathMoney);
            if (amount <= 0 || money <= 0) {
                return;
            }
            if (amount > money) {
                amount = money;
            }
            economy.withdrawPlayer(event.getEntity(), amount);
            String death = plugin.getConfig().getString("settings.death")
                    .replace("$amount", amount.toString()).replace("&", "§");
            if (!death.equals("")) {
                event.getEntity().sendMessage(death);
            }
        }
    }

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        if (player.hasPermission("combatmanager.bypass")) {
            return;
        }
		List<String> worlds = plugin.getConfig().getStringList("settings.worlds");
        if (worlds.contains(player.getLocation().getWorld().getName()) || worlds.contains("ALL")) {
            if (combatManager.getCombat().containsKey(player)) {
                List<String> commandList = plugin.getConfig().getStringList("settings.commandList");
                Boolean commandWhitelist = plugin.getConfig().getBoolean("settings.commandWhitelist");
                String commandCombat = plugin.getConfig().getString("settings.commandCombat").replace("&", "§");
                String commandCombatTitle = plugin.getConfig().getString("settings.commandCombatTitle")
                        .replace("&", "§");
                String commandCombatSubtitle = plugin.getConfig().getString("settings.commandCombatSubtitle")
                        .replace("&", "§");
                player.sendMessage(event.getMessage());
                if (commandList.contains(event.getMessage().substring(1).toLowerCase())) {
                    if (!commandWhitelist) {
                        event.setCancelled(true);
                        if (!commandCombat.equals("")) {
                            player.sendMessage(commandCombat);
                        }
                        if (!commandCombatTitle.equals("")) {
                            player.sendTitle(commandCombatTitle, commandCombatSubtitle, 10, 20, 10);
                        }
                    }
                } else {
                    event.setCancelled(true);
                    if (!commandCombat.equals("")) {
                        player.sendMessage(commandCombat);
                    }
                    if (!commandCombatTitle.equals("")) {
                        player.sendTitle(commandCombatTitle, commandCombatSubtitle, 10, 20, 10);
                    }
                }
            }
        }
    }
}
