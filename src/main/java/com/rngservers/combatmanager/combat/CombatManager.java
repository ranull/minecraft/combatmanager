package com.rngservers.combatmanager.combat;

import com.rngservers.combatmanager.Main;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class CombatManager {
    private Main plugin;
    private Economy economy;
    private Map<Player, Long> combatList = new HashMap<>();

    public CombatManager(Main plugin, Economy economy) {
        this.plugin = plugin;
        this.economy = economy;
        combatChecker();
        flyChecker();
    }

    public void combatChecker() {
        new BukkitRunnable() {
            @Override
            public void run() {
                for (Iterator<Map.Entry<Player, Long>> iterator = combatList.entrySet()
                        .iterator(); iterator.hasNext(); ) {
                    Map.Entry<Player, Long> entry = iterator.next();

                    Integer combatFor = plugin.getConfig().getInt("settings.combatTime") * 1000;

                    Player player = entry.getKey();
                    Long time = entry.getValue();

                    Long diff = System.currentTimeMillis() - time;
                    if (diff >= combatFor) {
                        iterator.remove();
                        removeCombat(player);
                    }
                }
            }
        }.runTaskTimer(plugin, 0L, 1L);
    }

    public void flyChecker() {
        new BukkitRunnable() {
            @Override
            public void run() {
                Boolean fly = plugin.getConfig().getBoolean("settings.fly");
                if (fly) {
                    return;
                }
                for (Iterator<Map.Entry<Player, Long>> iterator = combatList.entrySet()
                        .iterator(); iterator.hasNext(); ) {
                    Map.Entry<Player, Long> entry = iterator.next();

                    if (entry.getKey().isFlying()) {
                        entry.getKey().setFlying(false);
                    }
                }
            }
        }.runTaskTimer(plugin, 0L, 1L);
    }

    public void addCombat(Player player) {
        if (!combatList.containsKey(player)) {
            Integer combatTime = plugin.getConfig().getInt("settings.combatTime");
            String addCombat = plugin.getConfig().getString("settings.addCombat")
                    .replace("$time", combatTime.toString()).replace("&", "§");
            if (!addCombat.equals("")) {
                player.sendMessage(addCombat);
            }
            String addCombatTitle = plugin.getConfig().getString("settings.addCombatTitle")
                    .replace("$time", combatTime.toString()).replace("&", "§");
            String addCombatSubtitle = plugin.getConfig().getString("settings.addCombatSubtitle")
                    .replace("$time", combatTime.toString()).replace("&", "§");
            if (!addCombatTitle.equals("")) {
                player.sendTitle(addCombatTitle, addCombatSubtitle, 10, 20, 10);
            }
        }
        combatList.put(player, System.currentTimeMillis());
        if (player.isFlying()) {
            Boolean fly = plugin.getConfig().getBoolean("settings.fly");
            if (!fly) {
                player.setFlying(false);
            }
        }
    }

    public void removeCombat(Player player) {
        combatList.remove(player);
        String removeCombat = plugin.getConfig().getString("settings.removeCombat")
                .replace("&", "§");
        if (!removeCombat.equals("")) {
            player.sendMessage(removeCombat);
        }
        String removeCombatTitle = plugin.getConfig().getString("settings.removeCombatTitle")
                .replace("&", "§");
        String removeCombatSubtitle = plugin.getConfig().getString("settings.removeCombatSubtitle")
                .replace("&", "§");
        if (!removeCombatTitle.equals("")) {
            player.sendTitle(removeCombatTitle, removeCombatSubtitle, 10, 20, 10);
        }
    }

    public Map<Player, Long> getCombat() {
        return combatList;
    }

    public Double getMoneyAmount(Player player, String string) {
        if (string.contains("%")) {
            Double percent = Double.valueOf(string.replace("%", ""));
            Double money = economy.getBalance(player);
            if (money <= 0) {
                return 0.0;
            }
            return Math.round((money * percent) * 100.0) / 100.0;
        } else if (string.contains("-")) {
            String[] split = string.split("-");
            Double min = Double.valueOf(split[0]);
            Double max = Double.valueOf(split[1]);
            Double amount = min + (max - min) * new Random().nextDouble();
            return Math.round(amount * 100.0) / 100.0;
        } else {
            Double amount = Double.valueOf(string);
            return amount;
        }
    }

    public void dropPlayer(Player player) {
        if (plugin.getConfig().getStringList("settings.gamemode").contains(player.getGameMode().toString())) {
            if (plugin.getConfig().getBoolean("settings.dropInventory")) {
                dropInventory(player);
            }
            if (plugin.getConfig().getBoolean("settings.dropArmor")) {
                dropArmor(player);
            }
            if (plugin.getConfig().getBoolean("settings.dropEXP")) {
                dropEXP(player);
            }
        }
    }

    public void killPlayer(Player player) {
        if (plugin.getConfig().getStringList("settings.gamemode").contains(player.getGameMode().toString())) {
            if (plugin.getConfig().getBoolean("settings.killPlayer")) {
                player.setHealth(0);
            }
        }
    }

    public void dropInventory(Player player) {
        int counter = 0;
        for (ItemStack item : player.getInventory().getContents()) {
            if (counter >= 36) {
                return;
            }
            counter++;
            if (item != null) {
                if (plugin.getConfig().getStringList("settings.blacklist").contains(item.getType().toString())) {
                    continue;
                }
                player.getWorld().dropItemNaturally(player.getLocation(), item);
                player.getInventory().remove(item);
            }
        }
    }

    public void dropArmor(Player player) {
        ItemStack[] armor = player.getInventory().getArmorContents();
        for (ItemStack item : armor) {
            if (item != null) {
                if (plugin.getConfig().getStringList("settings.blacklist").contains(item.getType().toString())) {
                    continue;
                }
                player.getWorld().dropItemNaturally(player.getLocation(), item);
                item.setAmount(0);
            }
        }
        player.getInventory().setArmorContents(armor);
    }

    public void dropEXP(Player player) {
        player.setTotalExperience(0);
        player.getWorld().spawn(player.getLocation(), ExperienceOrb.class).setExperience(player.getTotalExperience());
    }
}
